from __future__ import absolute_import, unicode_literals

from mayan.apps.appearance.classes import Icon

icon_analyzer = Icon(driver_name='fontawesome', symbol='cogs')
