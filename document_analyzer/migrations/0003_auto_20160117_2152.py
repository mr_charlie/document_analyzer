# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0027_auto_20150824_0702'),
        ('document_analyzer', '0002_auto_20160116_1620'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='analyzer',
            name='document_version',
        ),
        migrations.AddField(
            model_name='analyzer',
            name='document_types',
            field=models.ManyToManyField(related_name='analyzers', verbose_name='Document types', to='documents.DocumentType'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='result',
            name='analyzer',
            field=models.ForeignKey(related_name='analyzer', verbose_name='Analyzer', to='document_analyzer.Analyzer'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='result',
            name='parameter',
            field=models.CharField(max_length=255, verbose_name='Parameter'),
            preserve_default=True,
        ),
    ]
