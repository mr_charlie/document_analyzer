from __future__ import unicode_literals

from __future__ import unicode_literals

__author__ = 'Matthias Loeblich'
__build__ = 0x010000
__copyright__ = 'Copyright 2015 Matthias Loeblich'
__license__ = 'MIT'
__title__ = 'document_analyzer'
__version__ = '1.0.0'

default_app_config = 'document_analyzer.apps.DocumentAnalyzerApp'
