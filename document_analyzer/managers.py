from __future__ import unicode_literals

import logging

from django.db import models
from django.utils.module_loading import import_string
from mayan.apps.documents.models import Document

logger = logging.getLogger(__name__)


class AnalyzerResultManager(models.Manager):

    def process_version(self, document_version):

        self.clean_version(document_version)

        for analyzer in document_version.document.document_type.analyzers.all():
            analyzer_class = import_string(analyzer.type)
            logger.debug("Initializing analyzer:{}".format(analyzer_class))
            backend = analyzer_class()
            result = backend.execute(document_version=document_version, parameter=analyzer.parameter)
            logger.debug("Result:{} from analyzer:{}".format(result, analyzer_class))
            for parameter, value in result:
                self.create(
                    analyzer=analyzer, document_version=document_version, parameter=parameter, value=value
                )

    def clean_version(self, document_version):
        document_version.analyzerresult.all().delete()

    def rebuild_all_anaylzers(self):
        for result in self.all():
            result.delete()

        for document in Document.objects.all():
            self.process_version(document.latest_version)
